# sample-vue-game

Vue.jsで作成した、サンプルゲーム・・・の種のようなものです。
斜めに揃わない [Connect 4](https://www.google.com/search?q=connect+4) です。

[GitLab Pages](https://y-tosa.gitlab.io/sample-vue-game/) でホストしています。

## 起動までの手順

1. このリポジトリをクローンします。
1. [Node.js](https://nodejs.org/ja/download/) をインストールします。
1. [yarn](https://classic.yarnpkg.com/en/docs/install/) をインストールします。
1. （コマンドプロンプト等で）このディレクトリに移動し、 `yarn install` を実行します。
1. 以降、このディレクトリで `yarn serve` を実行することで、デバッグ実行できます。

## デプロイ方法

`.gitlab-ci.yml` の設定により、GitLabにプッシュしたときに限り、自動で [GitLab Pages](https://www.gitlab.jp/blog/2020/06/29/how-to-use-gitlab-pages/) にデプロイされます。

## 補足

このプロジェクトでは、以下を使用しています。

* [Vue CLI](https://cli.vuejs.org)
* [Vue.js 3](https://v3.vuejs.org/)
* [TypeScript](https://www.typescriptlang.org/)
* [webpack](https://webpack.js.org/)
