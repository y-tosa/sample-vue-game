import { PieceColor } from "@/ts/board";

export class Player {
  readonly playerNumber: number;
  readonly pieceColor: PieceColor;

  constructor(playerNumber: number, pieceColor: PieceColor) {
    this.playerNumber = playerNumber;
    this.pieceColor = pieceColor;
  }
}
