import _ from "lodash";

export enum PieceColor {
  Red,
  Blue
}

export class Piece {
  readonly x: number;
  readonly y: number;
  readonly color: PieceColor;

  constructor(x: number, y: number, color: PieceColor) {
    this.x = x;
    this.y = y;
    this.color = color;
  }
}

export class Cell {
  readonly x: number;
  readonly y: number;
  private _piece: Piece | null = null;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  setPiece(color: PieceColor) {
    this._piece = new Piece(this.x, this.y, color);
  }

  getPiece(): Piece | null {
    return this._piece;
  }

  isEmpty(): boolean {
    return this._piece === null;
  }

  isNotEmpty(): boolean {
    return !this.isEmpty();
  }

  hasRedPiece(): boolean {
    return this._piece?.color === PieceColor.Red ?? false;
  }

  hasBluePiece(): boolean {
    return this._piece?.color === PieceColor.Blue ?? false;
  }
}

export class Connection {
  readonly cells: Cell[];

  constructor(cells: Cell[]) {
    this.cells = cells;
  }
}

export class Board {
  readonly boardWidth: number;
  readonly boardHeight: number;
  readonly numPiecesToConnect: number;
  readonly cells: Cell[][]; // y -> x の順
  private _connections: Connection[] = [];

  constructor(
    boardWidth: number,
    boardHeight: number,
    numPiecesToConnect: number
  ) {
    this.cells = _.range(boardHeight).map(y =>
      _.range(boardWidth).map(x => new Cell(x, y))
    );
    this.boardWidth = boardWidth;
    this.boardHeight = boardHeight;
    this.numPiecesToConnect = numPiecesToConnect;
  }

  dropPiece(x: number, color: PieceColor): boolean {
    const y = this.getYIfDropped(x);
    if (y === null) {
      console.log("この列は既にいっぱいです。");
      return false;
    }
    this.cells[y][x].setPiece(color);
    this.updateConnections();
    return true;
  }

  /**
   * 指定したx座標に落としたときのy座標を取得する。
   * いっぱいで落とせない場合は、nullを返す。
   *
   * @param x
   */
  getYIfDropped(x: number): number | null {
    const column = this.cells.map(row => {
      const cell: Cell | undefined = row[x];
      if (cell === undefined)
        throw new Error("ボードの範囲外のx座標を指定しました。");
      return cell;
    });

    const y = _.findLastIndex(column, cell => cell.isEmpty());
    if (y < 0) return null;
    return y;
  }

  updateConnections() {
    this._connections = [...this.scanRows(), ...this.scanCols()];
  }

  private scanRows(): Connection[] {
    // 横方向
    const firstXMax = this.boardWidth - this.numPiecesToConnect + 1;
    if (firstXMax <= 0) return [];

    const connections: Connection[] = [];
    for (const row of this.cells) {
      for (const firstX of _.range(firstXMax)) {
        const cells = _.slice(row, firstX, firstX + this.numPiecesToConnect);
        if (haveAllSameColor(cells)) {
          connections.push(new Connection(cells));
        }
      }
    }

    return connections;
  }

  private scanCols(): Connection[] {
    // 縦方向
    const firstYMax = this.boardHeight - this.numPiecesToConnect + 1;
    if (firstYMax <= 0) return [];

    const connections: Connection[] = [];
    for (const x of _.range(this.boardWidth)) {
      const column = this.cells.map(row => row[x]);
      for (const firstY of _.range(firstYMax)) {
        const cells = _.slice(column, firstY, firstY + this.numPiecesToConnect);
        if (haveAllSameColor(cells)) {
          connections.push(new Connection(cells));
        }
      }
    }

    return connections;
  }

  get connections(): Connection[] {
    return this._connections;
  }
}

function haveAllSameColor(cells: Cell[]): boolean {
  const firstColor = cells[0].getPiece()?.color;
  if (firstColor === undefined) return false;
  for (const cell of cells) {
    const color = cell.getPiece()?.color;
    if (color === undefined || color !== firstColor) return false;
  }
  return true;
}
